#![allow(incomplete_features)]
#![feature(adt_const_params)]
use sycamore::prelude::*;
use pseudo_tilt::chern_character::{ChernChar, ChowGens, Δ, μ};
use pseudo_tilt::tilt_stability::{all_pseudo_semistabilizers, wall_centre};
use std::cmp::Ordering::Less;

const P1: ChowGens = ChowGens{a:1, b:1, c:1};

#[component]
fn App<G: Html>(cx: Scope) -> View<G> {
    const DEFAULT_R: i64 = 3;
    const DEFAULT_C: i64 = 2;
    const DEFAULT_D: i64 = -4;
    const P1: ChowGens = ChowGens{a:1, b:1, c:1};

    let r = create_signal(cx, DEFAULT_R);
    let c = create_signal(cx, DEFAULT_C);
    let d = create_signal(cx, DEFAULT_D);

    let rcd_value = create_signal(cx, ChernChar::<P1>{r:DEFAULT_R, c:DEFAULT_C, d:DEFAULT_D});

    // update rcd_value when r_input, c_input, or d_input change
    create_effect(cx, || {
        rcd_value.set(ChernChar::<P1>{
            r: *r.get(),
            c: *c.get(),
            d: *d.get(),
        });
    });

    view! { cx,
        div(class="container") {
            h1{"Pseudo-semistabilizers for Tilt Stability on Picard Rank 1 Surface" del{"s"} }
            details {
                summary{"What this does"}
                p {"Given a Chern character v, this app computes a superset of the Chern characters which give rise circular walls left of the vertical one (β=μ(v)). This is only attempted when we know there are finitely many, otherwise flags issue."}
                "That is, Chern characters u such that:"
                ul {
                    li {
                        "All appropriate Bogomolov-Gieseker inequalities are satisfied:"
                        ul {
                            li {"Δ(u) ≥ 0"}
                            li {"Δ(v-u) ≥ 0"}
                            li {"Δ(v) ≥ Δ(u)+Δ(v-u)"}
                        }
                    }
                    li {"The tilt slope of u surpasses that of v going 'inwards' (going down the left branch of the hyperbola ch₂^{α,β}(v)=0)"}
                }
            }
            div(class="chern-input") {
                "ch₀: "
                button(on:click=|_| {
                    if *r.get() > 1 {
                        r.set(*r.get()-1);
                    }
                }){"−"}
                span{ (r.to_string()) }
                button(on:click=|_| {
                    r.set(*r.get()+1);
                }){"+"}
                br{}
                "ch₁: "
                button(on:click=|_| {
                    c.set(*c.get()-1);
                }){"−"}
                span{ (c.to_string()) }
                button(on:click=|_| {
                    c.set(*c.get()+1);
                }){"+"}
                " ℓ"
                br{}
                "ch₂: "
                button(on:click=|_| {
                    d.set(*d.get()-1);
                }){"−"}
                span{ (d.to_string()) }
                button(on:click=|_| {
                    d.set(*d.get()+1);
                }){"+"}
                " ½ℓ²"
            }
            br{}
            div(class="output-display") {
                table {
                    (chern_info_tbody(cx, *rcd_value.get()))
                    (semistabilizer_tbody(cx, *rcd_value.get()))
                }
            }
        }
    }
}

#[component]
fn semistabilizer_tbody<G: Html>(cx: Scope, v: ChernChar::<P1>) -> View<G> {
    let semistabilizers_result = all_pseudo_semistabilizers(&v);

    let semistabs_ok = match semistabilizers_result {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    };

    let semistabs_list = create_signal(cx,
        semistabilizers_result.unwrap_or_default()
    );

    #[derive(PartialEq, Clone)]
    enum ColumnOrdering {
        Increasing,
        Decreasing,
        Ignored,
    }

    impl ColumnOrdering {
        fn toggle(&mut self) {
            if ColumnOrdering::Increasing == *self {
                *self = ColumnOrdering::Decreasing;
            } else {
                *self = ColumnOrdering::Increasing;
            }
        }

        fn switch_off(&mut self) {
            *self = ColumnOrdering::Ignored;
        }

        fn class_name(&self) -> String {
            match self {
                ColumnOrdering::Increasing => "increasing".to_string(),
                ColumnOrdering::Decreasing => "decreasing".to_string(),
                ColumnOrdering::Ignored => "ignored".to_string(),
            }
        }
    }

    let mu_ordering = create_signal(cx, ColumnOrdering::Ignored);
    let delta_ordering = create_signal(cx, ColumnOrdering::Ignored);
    let centre_ordering = create_signal(cx, ColumnOrdering::Ignored);

    match semistabs_ok {
        Ok(()) => {
            view! {cx,
                    tbody{
                        tr {
                            th(colspan = "4") {"Pseudo-semistabilizers on ℙ²:"}
                        }
                        tr {
                            th { "Chern character" }
                            th(
                                class=(delta_ordering.get().class_name()),
                                on:click=|_| {
                                    delta_ordering.modify().toggle();
                                    mu_ordering.modify().switch_off();
                                    centre_ordering.modify().switch_off();

                                    match *delta_ordering.get() {
                                        ColumnOrdering::Decreasing => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                Δ(a).cmp(&Δ(b))
                                            });
                                        },
                                        _ => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                Δ(b).cmp(&Δ(a))
                                            });
                                        }
                                    };
                                }
                            ) { "Bogomolov" }
                            th(
                                class=(mu_ordering.get().class_name()),
                                on:click=|_| {
                                    mu_ordering.modify().toggle();
                                    delta_ordering.modify().switch_off();
                                    centre_ordering.modify().switch_off();

                                    match *mu_ordering.get() {
                                        ColumnOrdering::Decreasing => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                μ(a).cmp(&μ(b))
                                            });
                                        },
                                        _ => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                μ(b).cmp(&μ(a))
                                            });
                                        }
                                    };
                                }
                            ) { "Mumford slope" }
                            th (
                                class=(centre_ordering.get().class_name()),
                                on:click= move |_| {
                                    centre_ordering.modify().toggle();
                                    delta_ordering.modify().switch_off();
                                    mu_ordering.modify().switch_off();

                                    match *centre_ordering.get() {
                                        ColumnOrdering::Decreasing => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                wall_centre(a, &v).cmp(&wall_centre(b, &v))
                                            });
                                        },
                                        _ => {
                                            semistabs_list.modify().sort_by(|a, b| {
                                                wall_centre(b, &v).cmp(&wall_centre(a, &v))
                                            });
                                        }
                                    };
                                }
                            ) {"Pseudo-wall centre"}
                        }
                        Keyed(
                            iterable=semistabs_list,
                            view= move |cx, u| view! { cx,
                                tr {
                                    td { (u) }
                                    td { "Δ= " (Δ(&u)) }
                                    td { "μ= " (μ(&u)) }
                                    td { (
                                      match wall_centre(&u, &v) {
                                        Some(val) => val.to_string(),
                                        None => "".to_string()
                                      }
                                    )}
                                }
                            },
                            key=|x| x.to_string(),
                        )
                    }
            }
        }
        Err(e) => {
            view! {cx,
                    tbody{
                        tr {
                            td(class="error", colspan = "3") {(e.to_string())}
                        }
                    }
            }
        }
    }
}

#[component]
fn chern_info_tbody<G: Html>(cx: Scope, v: ChernChar::<P1>) -> View<G> {
    view! {cx,
        tbody {
            tr {
                th(colspan = "3") {"Considered Chern:"}
            }
            tr {
                th { "Chern character" }
                th { "Bogomolov" }
                th { "Mumford slope" }
            }
            tr {
                td { (v.to_string()) }
                td( class=(match Δ(&v).cmp(&0) {
                        Less => "error",
                        _ => "",
                })) { "Δ= " (Δ(&v)) }
                td { "μ= " (μ(&v)) }
            }
        }
    }
}

fn main() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(log::Level::Debug).unwrap();

    sycamore::render(|cx| view! { cx, App {} });
}
