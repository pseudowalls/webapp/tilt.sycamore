Webapp wrapping around the [tilt.rs](https://gitlab.com/pseudowalls/tilt.rs) library for computing pseudo-semistabilizers for a given Chern character on $P^2$.

Currently hosted on gitlab pages [here](https://pseudowalls.gitlab.io/webapp/tilt.sycamore).

Currently crashes silently whenever the library panics :(
